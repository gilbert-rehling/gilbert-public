# Gilbert Rehling public snippet library

This library will contain multiple scripts, each containing small snippets of code that may be usefull for web develoment.

The primary purpose for this libray it to act as an olnie shareable repository for usefull bit of code.

** Version: 1.0 beta**

Contents:  
1) PHP  
2) JavaScript  
3) CSS  
4) Vue  
5) Unclassified  

I shall endeavour to create new content pages and or reorganise content a sI see fit.

**Author Website:** [www.gilbert-rehling.com](http://www.gilbert-rehling.com)

Copyright (c) 2019 Gilbert Rehling (no rights reserved)

