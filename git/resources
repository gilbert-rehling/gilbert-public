## Clear cache
# - single item
- git rm --cached .idea
# - or sequence to clear all
- git rm -r --cached .
- git add .
- git commit -am 'git cache cleared'
- git push
- https://stackoverflow.com/questions/41863484/clear-git-local-cache

## Command reference
https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-update-index.html

## Now that GIT has moved away from username:password https:// styled access, use this command to update the 'git remove -v' results
- git remote -v ## this shows your current remote
	- origin  https://github.com/php-mongo/installation-files.git (fetch)
	- origin  https://github.com/php-mongo/installation-files.git (push)
# We need to update this to use ssh access to be able to push woth our private:public key pair via SSH
- git remote set-url origin get@github.com:php-mongo/installation-files.git
	# Note the ~.com:php-mongo where 'php-mongo' defines the GIT username directly after the domain separated by a colon.
	- git remote add origin git@github.com:php-mongo/installation-files.git  # slightly different for alternative scenario
	
## Other useful commands
- git remote add origin https://github.com/php-mongo/docker-stand-alone.git # git allows you to pull from another remote branch
- git push --set-upstream origin testing # where 'testing' is a branch name
- git push -u origin staging
- git pull origin testing

# delete local branch
- git branch -d < branc name >
# delete remote reference
- git remote rm < branch name >

# Remove a file from the cache - this should remove it from repo history too
- git rm --cached <file path>
# Even better
- git filter-branch --index-filter 'rm -f Resources\Video\%font%.ttf' -- --all

- git remote prune < branch name > # requires that the remote branch has been otherwise deleted/removed

# To hide changes in order to 'pull' in new changes or for some other reason
# This will strash all changes since the last commit
- git stash

# To revert (rollback) commited changes or pulled changes to your prod repo and go back to the most recent
git reset --hard
# To revert back to a previous commit state - 2 commit ago
git reset --hard HEAD~2 || git rteset --hard HEAD@(2)

# To remove changes in working branch (uncommited)
git checkout . || git restore .

# To remove untracked files - the -d is for directories
git clean -f || git clean -fd
