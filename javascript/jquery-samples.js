JavaScript:

-// add passive non blocker to jQuery event listeners

-jQuery.event.special.touchstart = {

-    setup: function( _, ns, handle )

-    {

-        if ( ns.includes("noPreventDefault") )

-        {

-            this.addEventListener("touchstart", handle, { passive: false });

-        }

-        else

-        {

-            this.addEventListener("touchstart", handle, { passive: true });

-        }

-    }

-};

-

-// selector that can handle spaces in the DOM id attribute

-$('[id="' + field + '"]').val();

-$('[id="ID With Space"]').on('change', (e) => {

-    console.log("selected: " + $(this).val());

-});

-

-// jquery validatiion and error handling

-var scrollToError = false;

-// form validation function

-function initValidation() {

-	var errorClass = 'error';

-	var successClass = 'success';

-	var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

-	var regPhone = /^[0-9]+$/;

-

-	jQuery ('form.validate-form').each(function(){

-		var form = jQuery(this).attr('novalidate', 'novalidate');

-		var successFlag = true;

-		var inputs = form.find('input, textarea, select, .check-list, .radio-list');

-

-		// form validation function

-		function validateForm(e) {

-			successFlag = true;

-

-			inputs.each(checkField);

-

-			if (!successFlag) {

-				e.preventDefault();

-				scrollToError = false;

-			}

-		}

-

-		// check field

-		function checkField(i, obj) {

-			var currentObject = jQuery(obj);

-			var currentParent = currentObject.closest('.required-holder, .form-col');

-                        if (currentParent.hasClass('hidden') || currentParent.parents('.hidden').length > 0 ||

-                                currentObject.attr('disabled')) {

-                            return;

-                        }

-			// not empty fields

-			if (currentObject.hasClass('required')) {

-				setState(currentParent, currentObject, !currentObject.val().length);

-			}

-			// correct email fields

-			if (currentObject.hasClass('required-email')) {

-				setState(currentParent, currentObject, !regEmail.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('email')) {

-				setState(currentParent, currentObject, currentObject.val().length && !regEmail.test(currentObject.val()));

-			}

-			// correct number fields

-			if (currentObject.hasClass('required-number')) {

-				setState(currentParent, currentObject, !regPhone.test(currentObject.val()));

-			}

-			// something selected

-			if (currentObject.hasClass('required-select')) {

-				setState(currentParent, currentObject, currentObject.val() == '');

-			}

-			if (currentObject.hasClass('required-postcode')) {

-                            setState(currentParent, currentObject, ! /^\d{4}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('postcode')) {

-                            setState(currentParent, ! /^\d{4}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('required-street-suburb')) {

-                            setState(currentParent, currentObject, ! /^[a-zA-Z\s'\-\/\\#&*,.]{1,40}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('required-acnabn')) {

-                            setState(currentParent, currentObject, ! (/^\d{9}$/.test(currentObject.val()) || /^\d{11}$/.test(currentObject.val())));

-			}

-			if (currentObject.hasClass('street-suburb')) {

-                            setState(currentParent, currentObject, ! /^[a-zA-Z\s'\-\/\\#&*,.]{0,40}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('required-street-no')) {

-                            setState(currentParent, currentObject, ! /^[0-9a-zA-Z\s'\-\/\\#&*,.]{1,8}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('street-no')) {

-                            setState(currentParent, currentObject, ! /^[0-9a-zA-Z\s'\-\/\\#&*,.]{0,8}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('flat-no')) {

-                            setState(currentParent, currentObject, ! /^[0-9a-zA-Z\s'\-\/\\#&*,.]{0,8}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('required-agencylicence')) {

-                			setState(currentParent, currentObject, ! /^[0-9a-zA-Z]{1,25}$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('namefield')) {

-                            setState(currentParent, currentObject, currentObject.val().length && !/^[a-zA-Z]+[a-zA-Z-' ]*$/.test(currentObject.val()));

-			}

-			if (currentObject.hasClass('required-date')) {

-				var value = currentObject.val().replace(/\s+/g, '');

-				setState(currentParent, currentObject,  !/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value) );

-			}

-			if (currentObject.hasClass('required-namefield')) {

-				setState(currentParent, currentObject, !currentObject.val().length || !/^[a-zA-Z]+[a-zA-Z-' ]*$/.test(currentObject.val()));

-			}

-			if (currentParent.hasClass('required-checkbox')) {

-				setState(currentParent, currentObject, !currentParent.find(':checkbox:checked').length);

-			}

-			if (currentObject.hasClass('required-phone')) {

-				var value = currentObject.val().replace(/\s+/g, '');

-				setState(currentParent, currentObject, !/^[\(]?0[2-9]{1}[\)]?[0-9]{8}$/.test(value) || !/(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/.test(value));

-			}

-			if (currentObject.hasClass('phone')) {

-				var value = currentObject.val().replace(/\s+/g, '');

-				setState(currentParent, currentObject, value.length && (!/^[\(]?0[2-9]{1}[\)]?[0-9]{8}$/.test(value) || !/(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/.test(value)));

-			}

-		}

-

-		// set state

-		function setState(hold, field, error) {

-			hold.removeClass(errorClass).removeClass(successClass);

-			if (error) {

-                hold.addClass(errorClass);

-				field.one('focus',function(){hold.removeClass(errorClass).removeClass(successClass);});

-				successFlag = false;

-				if (!scrollToError) {

-					scrollToError = true;

-				$('html, body').animate({ scrollTop: hold.offset().top - 50}, 1000);

-				}

-			} else {

-				hold.addClass(successClass);

-			}        

-		}

-

-		// form event handlers

-		form.submit(validateForm);

-	});

-}

-

-// form validation function

-function initValidation() {

-	var errorClass = 'error';

-	var successClass = 'success';

-	var rowValidateClass = 'validate-row';

-	var validateItemClass = 'validate-item';

-	var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

-	var regDate = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{2,4}$/;

-	var regPhone = /^[0-9]{8,}$/;

-	var regNumber  = /^[0-9]+$/;

-

-	jQuery('form.validate-form').each(function() {

-		var form = jQuery(this).attr('novalidate', 'novalidate');

-		var successFlag = true;

-		var inputs = form.find('input, textarea, select');

-		var links = form.find('.open-close1 .opener1');

-

-		links.on('click', function() {

-			var activeRows = form.find('.open-close1.active').find('.' + validateItemClass);

-			if (activeRows.length) {

-				form.find('.open-close1:not(.active)').find('.' + validateItemClass).removeClass(rowValidateClass).removeClass(errorClass);

-				activeRows.addClass(rowValidateClass);

-			}

-		});

-

-		// form validation function

-		function validateForm(e) {

-

-			console.log("validating form!");

-

-			successFlag = true;

-

-			inputs.each(checkField);

-

-			if (!successFlag) {

-				e.preventDefault();

-				return;

-			}

-			// else, show waiting screen

-			waiting();

-		}

-

-		// check field

-		function checkField(i, obj) {

-			var currentObject = jQuery(obj);

-			var currentParent = currentObject.closest('.' + rowValidateClass);

-

-			if (!currentParent.length) return;

-			

-			// KT 

-        	if (currentObject.hasClass('commercial')) {

-        		if (currentObject.is(":hidden")) return;

-			}  

-

-			// not empty fields

-			if (currentObject.hasClass('required')) {

-				setState(currentParent, currentObject, !currentObject.val().length);

-			}

-			// correct email fields

-			if (currentObject.hasClass('required-email')) {

-				setState(currentParent, currentObject, !regEmail.test(currentObject.val()));

-			}

-			// correct date fields

-			if (currentObject.hasClass('required-date')) {

-					setState(currentParent, currentObject, !regDate.test(currentObject.val()));

-			}

-			// correct future date fields

-			if (currentObject.hasClass('required-birthday')) {

-				var now = new Date;

-				var target = new Date(currentObject.val());

-				setState(currentParent, currentObject, target > now || !regDate.test(currentObject.val()));

-			}

-			// correct number fields

-			if (currentObject.hasClass('required-number')) {

-				var value = currentObject.val().replace(/\s+/g, '');

-				setState(currentParent, currentObject, !regNumber.test(value));

-			}

-			// correct phone number fields

-			if (currentObject.hasClass('required-phone')) {

-				var value = currentObject.val().replace(/\s+/g, '');

-				//setState(currentParent, currentObject, !regPhone.test(value));

-				setState(currentParent, currentObject, !validatePhone(value));

-			}

-			// something selected

-			if (currentObject.hasClass('required-select')) {

-				setState(currentParent, currentObject, currentObject.get(0).selectedIndex === 0);

-			}

-			// radio field

-			if (currentParent.hasClass('required-radio')) {

-				setState(currentParent, currentObject, !currentParent.find(':radio:checked').length);

-			}

-			if (currentObject.hasClass('validate-phone')) {

-				var value = currentObject.val().replace(/\s+/g, '');

-				if (value.length >= 1) {

-					console.log("validate phone: " + value);

-					setState(currentParent, currentObject, !validatePhone(value));

-				}

-			}

-		}

-

-		// set state

-		function setState(hold, field, error) {

-			hold.removeClass(errorClass).removeClass(successClass);

-			if (error) {

-				hold.addClass(errorClass);

-				field.one('focus', function() {

-					hold.removeClass(errorClass).removeClass(successClass);

-				});

-				successFlag = false;

-			} else {

-				hold.addClass(successClass);

-			}

-		}

-

-		// form event handlers

-		form.submit(validateForm);

-	});

-}

-

-/*

- * jQuery SameHeight plugin

- */

-(function($){

-	$.fn.sameHeight = function(opt) {

-		var options = $.extend({

-			skipClass: 'same-height-ignore',

-			leftEdgeClass: 'same-height-left',

-			rightEdgeClass: 'same-height-right',

-			elements: '>*',

-			flexible: false,

-			multiLine: false,

-			useMinHeight: false,

-			biggestHeight: false

-		},opt);

-		return this.each(function(){

-			var holder = $(this), postResizeTimer, ignoreResize;

-			var elements = holder.find(options.elements).not('.' + options.skipClass);

-			if(!elements.length) return;

-

-			// resize handler

-			function doResize() {

-				elements.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', '');

-				if(options.multiLine) {

-					// resize elements row by row

-					resizeElementsByRows(elements, options);

-				} else {

-					// resize elements by holder

-					resizeElements(elements, holder, options);

-				}

-			}

-			doResize();

-

-			// handle flexible layout / font resize

-			var delayedResizeHandler = function() {

-				if(!ignoreResize) {

-					ignoreResize = true;

-					doResize();

-					clearTimeout(postResizeTimer);

-					postResizeTimer = setTimeout(function() {

-						doResize();

-						setTimeout(function(){

-							ignoreResize = false;

-						}, 10);

-					}, 100);

-				}

-			};

-

-			// handle flexible/responsive layout

-			if(options.flexible) {

-				$(window).bind('resize orientationchange fontresize', delayedResizeHandler);

-			}

-

-			// handle complete page load including images and fonts

-			$(window).bind('load', delayedResizeHandler);

-		});

-	};

-

-	// detect css min-height support

-	var supportMinHeight = typeof document.documentElement.style.maxHeight !== 'undefined';

-

-	// get elements by rows

-	function resizeElementsByRows(boxes, options) {

-		var currentRow = $(), maxHeight, maxCalcHeight = 0, firstOffset = boxes.eq(0).offset().top;

-		boxes.each(function(ind){

-			var curItem = $(this);

-			if(curItem.offset().top === firstOffset) {

-				currentRow = currentRow.add(this);

-			} else {

-				maxHeight = getMaxHeight(currentRow);

-				maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));

-				currentRow = curItem;

-				firstOffset = curItem.offset().top;

-			}

-		});

-		if(currentRow.length) {

-			maxHeight = getMaxHeight(currentRow);

-			maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));

-		}

-		if(options.biggestHeight) {

-			boxes.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', maxCalcHeight);

-		}

-	}

-

-	// calculate max element height

-	function getMaxHeight(boxes) {

-		var maxHeight = 0;

-		boxes.each(function(){

-			maxHeight = Math.max(maxHeight, $(this).outerHeight());

-		});

-		return maxHeight;

-	}

-

-	// resize helper function

-	function resizeElements(boxes, parent, options) {

-		var calcHeight;

-		var parentHeight = typeof parent === 'number' ? parent : parent.height();

-		boxes.removeClass(options.leftEdgeClass).removeClass(options.rightEdgeClass).each(function(i){

-			var element = $(this);

-			var depthDiffHeight = 0;

-			var isBorderBox = element.css('boxSizing') === 'border-box' || element.css('-moz-box-sizing') === 'border-box' || element.css('-webkit-box-sizing') === 'border-box';

-

-			if(typeof parent !== 'number') {

-				element.parents().each(function(){

-					var tmpParent = $(this);

-					if(parent.is(this)) {

-						return false;

-					} else {

-						depthDiffHeight += tmpParent.outerHeight() - tmpParent.height();

-					}

-				});

-			}

-			calcHeight = parentHeight - depthDiffHeight;

-			calcHeight -= isBorderBox ? 0 : element.outerHeight() - element.height();

-

-			if(calcHeight > 0) {

-				element.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', calcHeight);

-			}

-		});

-		boxes.filter(':first').addClass(options.leftEdgeClass);

-		boxes.filter(':last').addClass(options.rightEdgeClass);

-		return calcHeight;

-	}

-}(jQuery));

-

-// normalize event

-		var fixEvent = function(e) {

-			var origEvent = e || window.event,

-				touchEventData = null,

-				targetEventName = eventMap[origEvent.type];

-

-			e = $.event.fix(origEvent);

-			e.type = eventPrefix + targetEventName;

-

-			if (origEvent.pointerType) {

-				switch (origEvent.pointerType) {

-					case 2: e.pointerType = 'touch'; break;

-					case 3: e.pointerType = 'pen'; break;

-					case 4: e.pointerType = 'mouse'; break;

-					default: e.pointerType = origEvent.pointerType;

-				}

-			} else {

-				e.pointerType = origEvent.type.substr(0, 5); // "mouse" or "touch" word length

-			}

-

-			if (!e.pageX && !e.pageY) {

-				touchEventData = origEvent.changedTouches ? origEvent.changedTouches[0] : origEvent;

-				e.pageX = touchEventData.pageX;

-				e.pageY = touchEventData.pageY;

-			}

-

-			if (origEvent.type === 'touchend') {

-				lastTouch = { x: e.pageX, y: e.pageY };

-			}

-			if (e.pointerType === 'mouse' && lastTouch && mouseEventSimulated(e)) {

-				return;

-			} else {

-				return ($.event.dispatch || $.event.handle).call(this, e);

-			}

-		};

-		

-// custom mousewheel/trackpad handler

-	(function() {

-		var wheelEvents = ('onwheel' in document || document.documentMode >= 9 ? 'wheel' : 'mousewheel DOMMouseScroll').split(' '),

-			shimEventName = 'jcf-mousewheel';

-

-		$.event.special[shimEventName] = {

-			setup: function() {

-				var self = this;

-				$.each(wheelEvents, function(index, fallbackEvent) {

-					if (self.addEventListener) self.addEventListener(fallbackEvent, fixEvent, false);

-					else self['on' + fallbackEvent] = fixEvent;

-				});

-			},

-			teardown: function() {

-				var self = this;

-				$.each(wheelEvents, function(index, fallbackEvent) {

-					if (self.addEventListener) self.removeEventListener(fallbackEvent, fixEvent, false);

-					else self['on' + fallbackEvent] = null;

-				});

-			}

-		};

-

-		var fixEvent = function(e) {

-			var origEvent = e || window.event;

-			e = $.event.fix(origEvent);

-			e.type = shimEventName;

-

-			// old wheel events handler

-			if ('detail'      in origEvent) { e.deltaY = -origEvent.detail;      }

-			if ('wheelDelta'  in origEvent) { e.deltaY = -origEvent.wheelDelta;  }

-			if ('wheelDeltaY' in origEvent) { e.deltaY = -origEvent.wheelDeltaY; }

-			if ('wheelDeltaX' in origEvent) { e.deltaX = -origEvent.wheelDeltaX; }

-

-			// modern wheel event handler

-			if ('deltaY' in origEvent) {

-				e.deltaY = origEvent.deltaY;

-			}

-			if ('deltaX' in origEvent) {

-				e.deltaX = origEvent.deltaX;

-			}

-

-			// handle deltaMode for mouse wheel

-			e.delta = e.deltaY || e.deltaX;

-			if (origEvent.deltaMode === 1) {

-				var lineHeight = 16;

-				e.delta *= lineHeight;

-				e.deltaY *= lineHeight;

-				e.deltaX *= lineHeight;

-			}

-

-			return ($.event.dispatch || $.event.handle).call(this, e);

-		};

-	}());

-

-// strict password management

-function checkPasswordsEqual() {

-	var pwd1 = $('#password').val(), pwd2 = $('#confirm-password').val(), cn = $("#clientnumber").val(), hasError = null, errorStr = '', count, i, arr, reg;

-	$(".alert-danger").hide();

-	if (pwd1.length == 0 || pwd1 == '') {

-		// set the alert

-		$(".js-alert").html('Your passwords are empty. Please try again.').show();

-		return false;

-	}

-	if (pwd1 != pwd2) {

-		// set the alert

-		$(".js-alert").html('Your passwords do not match. Please try again.').show();

-		return false;

-	}

-	// run some password validations

-	// min length

-	if (pwd1.length < 8) {

-		hasError = true;

-		errorStr += '<br>- must be at least 8 characters in length.';

-	}

-	// uppercase present

-	if ( pwd1.match(/[A-Z]/g) == null ) {

-		hasError = true;

-		errorStr += '<br>- must contain at least 1 uppercase character [A-Z].';

-	}

-	// lowercase present

-	if ( pwd1.match(/[a-z]/g) == null ) {

-		hasError = true;

-		errorStr += '<br>- must contain at least 1 lowercase character [a-z].';

-	}

-	// digit present

-	if ( pwd1.match(/[0-9]/g) == null ) {

-		hasError = true;

-		errorStr += '<br>- must contain at least 1 digit [0-9].';

-	}

-	// special chars

-	if ( pwd1.match(/[!()@#$%&?*:=+-]/g) == null ) {

-		hasError = true;

-		errorStr += '<br>- must contain at least 1 special character [!()@#$%&?*:=+-].';

-	}

-	// NO client iD allowed

-	reg = new RegExp(cn, 'i');

-	if ( pwd1.indexOf(cn) != -1 || pwd1.search(reg) != -1 ) {

-		hasError = true;

-		errorStr += '<br>- cannot contain your username.';

-	}

-	// consecutive chars limit

-	count = letterCount( pwd1 );

-	for (i in count) {

-		arr = count[i];

-		if (arr[1] >= 3) {

-			hasError = true;

-			errorStr += '<br>- cannot contain more than 2 consecutive characters [aa-11]';

-			break;

-		}

-	}

-

-	// if errors

-	if (hasError) {

-		$(".js-alert").hide();

-		$(".error-block").html('<p style="text-align: left; margin-bottom: 0">Please apply the following password rules:' + errorStr + '</p>');

-		$(".error-block").show();

-		return false;

-	}

-	return true;

-}

-

-function validatePhone(pnumber) {

-    pnumber = pnumber.split(' ').join('');

-

-    var pattern = /^[\(]?0[2-9]{1}[\)]?[0-9]{8}$/;

-    var pattern2 = /(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/;

-    // no empty number accepted

-    if(pattern.test(pnumber) || pattern2.test(pnumber)) {

-        return true;

-    } else {

-        return false;

-    }

-}

-

-findElements: function() {

-	this.regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

-	this.regNumber = /^[0-9]+$/;

-	this.alphanumeric = /^[a-zA-Z0-9]+$/;

-	//                        this.regPhone = /^[0-9]{8,}$/;

-	this.regPhone = /^[\(]?0[2-9]{1}[\)]?[0-9]{8}$/;

-	this.regPhone2 = /(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/;

-	this.regDate = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{2,4}$/;

-	// this.regDate = /(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;

-	this.regMonth = /^(0[1-9]|1[0-2])\/(19|20)\d{2}$/;

-	this.regName = /^[a-zA-Z]+[a-zA-Z-' ]*$/;

-	this.money = /^[1-9]{1}[0-9]*(\.[0-9]{1,2})?$/;

-	this.holder = $(this.options.holder);

-	this.slides = this.holder.find(this.options.slides);

-	this.slidesHolder = this.slides.eq(0).parent();

-	this.stepsCount = this.slides.length;

-	this.btnPrev = this.holder.find(this.options.btnPrev);

-	this.btnNext = this.holder.find(this.options.btnNext);

-	this.pagerLinks = this.holder.find(this.options.pagerLinks);

-	this.form = this.holder.find('form');                        

-	if(this.options.currentStep) {

-		this.currentStep = this.options.currentStep;

-	} else {

-		this.currentStep = 0;

-	}

-

-	var activeSlide = this.slides.filter('.' + this.options.activeClass);

-

-	if (activeSlide.length) {

-		this.currentStep = this.slides.index(activeSlide);

-	}

-	this.prevIndex = this.currentStep;

-},

-

-function isNameSearch() {

-    if( $('input[name="firstname"]').val() != '' || $('input[name="lastname"]').val() != '' )

-    {

-        return true;

-    }

-    return false;

-}

-

-jQuery.validator.addMethod(

-  "selectDropDown",

-  function(value, element) {

-	if (element.value == "")

-    {

-      return false;

-    }

-    else return true;

-  },

-  "Please select an option."

-);

-

-

-

-jQuery.validator.addMethod(

-  "noShellChars",

-  function(value, element) {

-    var pattern = /[!><\$\^\#\%\(\)]+|(--)/;

-    if(pattern.test(element.value))

-    {

-        return false;

-    }

-    else {

-      return true;

-    }

-  },

-  "Please enter letters and/or numbers"

-);

-

-jQuery.validator.addMethod(

-  "PlaceOfBirth",

-  function(value, element) {

-    var pattern = /[!><\$\^\#\%\(\)]+|(--)/;

-    if(pattern.test(element.value))

-    {

-        return false;

-    }

-    else {

-      return true;

-    }

-  },

-  "Please enter letters and/or numbers"

-);

-

-jQuery.validator.addMethod(

-  "validMoney",

-  function(value, element) {

-    var pattern = /^[1-9]{1}[0-9]*(\.[0-9]{1,2})?$/;

-    if(pattern.test(element.value) || element.value == '')

-    {

-        return true;

-    }

-    else {

-      return false;

-    }

-  },

-  "Please enter a valid amount"

-);

-

-jQuery.validator.addMethod(

-  "validatePhone",

-  function(value, element) {

-    var pnumber = element.value;

-    pnumber = pnumber.split(' ').join('');

-

-    var pattern = /^[\(]?0[2-9]{1}[\)]?[0-9]{8}$/;

-    var pattern2 = /(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/;

-    if(pattern.test(pnumber) || pattern2.test(pnumber) || pnumber == '')

-    {

-      return true;

-    }

-    else {

-        return false;

-    }

-  },

-  "Incorrect phone number format"

-);

-

-jQuery.validator.addMethod(

-  "validFullName",

-  function(value, element) {

-    var pattern = /^[a-zA-Z]+[a-zA-Z-' ]+$/;

-    if(pattern.test(element.value) || element.value =='')

-    {

-      return true;

-    }

-    else {

-        return false;

-    }

-  },

-  "Please enter a valid name"

-);

-

-function set_id_types() {

-	var str = $('#idtypex').val();

-	if( str == 'Australian Drivers License' )

-    	$('#driver').val( $('#idnum').val());

-	else if( str == 'New Zealand Drivers License' )

-    	$('#nzdriver').val( $('#idnum').val());

-	else if( str == 'Passport' ) {

-		if (!$('#idnum').val()) {

-			$('#passport').val( $('#passnum').val());

-		} else {

-			$('#passport').val( $('#idnum').val());

-		}

-	}

-}

-

-function countrySelectedForState() {

-	if ($('#country').val() == "Australia" )  {

-		return true;

-	}

-	else {

-		return false;

-	}

-}

-

-function rentedCountrySelectedForState() {

-	if ($('#rentedcountry').val() == "Australia")  {

-		return true;

-	}

-	else {

-		return false;

-	}

-}

-

-function countrySelected() {

-	if ($('#country').val() != "")  {

-		return true;

-	}

-	else {

-		return false;

-	}

-}

-

-jQuery.validator.addMethod(

-  "validDateOfBirth",

-  function(value, element) {

-

-	var ec = { dateErrorString : "zozzzo" };

-	var input_date = element.value;

-	if( input_date == '__/__/____' ) input_date = '';

-

-	if(isNameSearch() == true && input_date == '' )

-	{

-        ec.dateErrorString = "This is a required field";

-        $.validator.messages.validDateOfBirth = ec.dateErrorString;

-		return false;

-	}

-	else if( isNameSearch() == false && input_date == '' ){

-		return true;

-	}

-

-    var dob   = Date.parse(input_date);

-    var today = Date.today();

-

-    if( dob == null )

-    {

-        ec.dateErrorString = "Please enter a valid date";

-        $.validator.messages.validNTDDateOfBirth = ec.dateErrorString;

-        return false;

-    }

-    else if(dob > Date.today())

-    {

-        ec.dateErrorString = "Please enter a date that is not in the future";

-        $.validator.messages.validDateOfBirth = ec.dateErrorString;

-        return false;

-    }

-    else if( (18).years().ago() < dob )

-    {

-        ec.dateErrorString = "Tenant's age should be above 18 years";

-        $.validator.messages.validDateOfBirth = ec.dateErrorString;

-        return false;

-    }

-

-    $('#dayofbirth').val(dob.getDate());

-    $('#monthofbirth').val(dob.getMonth() + 1);

-    $('#yearofbirth').val(dob.getFullYear());

-

-    return true;

-  },

-  "Error :" + "Please enter a valid date"

-);

-

-jQuery.validator.addMethod(

-  "validAusLicense",

-  function(value, element) {

-	/*

-	 * I can cram all the patterns into one, but for readability's sake ....

-	*/

-	var patternSA_TAS = /^[A-Z]{2}[0-9]{4}$|^[A-Z]{1}[0-9]{5}$/i;

-	var patternNSW 	  = /^[0-9]{4,6}[A-Z]{1,2}$/i;

-	/*Covers newer (NSW & SA) and Other States, bloody NT is 'up to 7 digits' */

-

-	var patternAllDig = /^[0-9]{4,10}$/;

-

-	if( $('#idtypex').val() == 'Australian Drivers License' && element.value != '' )

-	{

-		if(patternSA_TAS.test(element.value) || patternNSW.test(element.value) || patternAllDig.test(element.value))

-		{

-        	return true;

-		}

-    	else {

-      		return false;

-    	}

-    }

-	else { return true; }

-  },

-  "Invalid Australian licence number format."

-);

-

-/** Admin password checking */

-            function letterCount(str) {

-                var s = str.match(/([a-zA-Z0-9])\1*/g)||[];

-                return s.map(function(itm){

-                    return [itm.charAt(0), itm.length];

-                });

-            }

-            function checkPasswordsEqual() {

-                var pwd1 = $('#password1').val(), pwd2 = $('#password2').val(), old = $('#old-password').val() | null, hasError = null, errorStr = '', count, i, arr, cn = '{$clientnumber}', patt = /{$clientnumber}/i;

-                if (old) {

-                    if (old.length == 0 || old == '') {

-                        $("div.js-error").html('Your current password is empty. Please try again.').show();

-                        return false;

-                    }

-                }

-                if (pwd1.length == 0 || pwd1 == '') {

-                    $("div.js-error").html('Your passwords are empty. Please try again.').show();

-                    return false;

-                }

-                if (pwd1 === pwd2) {

-                    // gtg

-                } else {

-                    // set the alert

-                    $("div.js-error").html('Your passwords do not match. Please try again.').show();

-                    return false;

-                }

-                // run some password validations

-                // min length

-                if (pwd1.length < 8) {

-                    hasError = true;

-                    errorStr += '<br>The password must be at least 8 characters in length.';

-                }

-                // uppercase present

-                if ( pwd1.match(/[A-Z]/g) == null ) {

-                    hasError = true;

-                    errorStr += '<br>The password must contain at least 1 uppercase character [A-Z].';

-                }

-                // lowercase present

-                if ( pwd1.match(/[a-z]/g) == null ) {

-                    hasError = true;

-                    errorStr += '<br>The password must contain at least 1 lowercase character [a-z].';

-                }

-                // digit present

-                if ( pwd1.match(/[0-9]/g) == null ) {

-                    hasError = true;

-                    errorStr += '<br>The password must contain at least 1 digit [0-9].';

-                }

-                // special chars

-                if ( pwd1.match(/[!()@#$%&?*:=+-]/g) == null ) {

-                    hasError = true;

-                    errorStr += '<br>The password must contain at least 1 special character [!()@#$%&?*:=+-].';

-                }

-                // client iD

-                if ( pwd1.match(patt) == cn ) {

-                    hasError = true;

-                    errorStr += '<br>The password may not contain an agent number.';

-                }

-                // consequtive chars limit

-                count = letterCount( pwd1 );

-                for (i in count) {

-                    arr = count[i];

-                    if (arr[1] >= 3) {

-                        hasError = true;

-                        errorStr += '<br>The password may not contain more than 2 consecutive characters [aa-11]';

-                        break;

-                    }

-                }

-

-                // if errors

-                if (hasError) {

-                    $("div.js-error").html('Please fix the following errors:' + errorStr).show();

-                    return false;

-                }

-                return true;

-            }

-			

-<p class="info-text small-text" style="margin-bottom: 0">

-	Password requirements:<br>

-	- at least 8 characters but less than 128 characters<br>

-	- contain at least 1 uppercase character [A-Z]<br>

-	- contain at least 1 digit [0-9]<br>

-	- contain at least 1 special character !()@#$%&?*:=+-<br>

-	- cannot contain the agent number<br>

-	- cannot contain more than 2 consecutive characters [aa-11]

-</p>			